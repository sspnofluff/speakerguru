﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dissonance.Networking
{
    internal sealed class ClientIdCollection
        : IReadonlyClientIdCollection
    {
        private readonly List<string> _items;
        private readonly Stack<ushort> _freeIds;
        private readonly IEnumerable<string> _alive;

        [NotNull] public IEnumerable<string> Items
        {
            get { return _alive; }
        }

        internal int Count
        {
            get { return _items.Count; }
        }

        internal string this[ushort id]
        {
            get { return _items[id]; }
        }

        public ClientIdCollection()
        {
            _items = new List<string>();
            _freeIds = new Stack<ushort>();
            _alive = _items.Where(x => x != null);
        }

        /// <summary>
        /// Get the name associated with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [CanBeNull] public string GetName(ushort id)
        {
            if (id >= _items.Count)
                return null;

            return _items[id];
        }

        /// <summary>
        /// Get the ID associated with the given name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ushort? GetId(string name)
        {
            for (ushort i = 0; i < _items.Count; i++)
            {
                if (_items[i] == name)
                    return i;
            }

            return null;
        }

        /// <summary>
        /// Add a new name and generate an ID
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ushort Register(string name)
        {
            var found = _items.IndexOf(name);
            if (found != -1)
                throw new InvalidOperationException(string.Format("Name is already in table with ID '{0}'", found));

            if (_freeIds.Count > 0)
            {
                var index = _freeIds.Pop();
                _items[index] = name;
                return index;
            }
            else
            {
                _items.Add(name);
                return (ushort)(_items.Count - 1);
            }
        }

        /// <summary>
        /// Remove the given name and free up it's ID for re-use
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Unregister(string name)
        {
            for (ushort i = 0; i < _items.Count; i++)
            {
                if (_items[i] == name)
                {
                    _items[i] = null;
                    _freeIds.Push(i);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Remove all items from the collection
        /// </summary>
        public void Clear()
        {
            _items.Clear();
            _freeIds.Clear();
        }

        public void Serialize(ref PacketWriter writer)
        {
            writer.Write((ushort)_items.Count);

            for (var i = 0; i < _items.Count; i++)
            {
                var item = _items[i];

                writer.Write(item);
            }
        }

        public void Deserialize(ref PacketReader reader)
        {
            Clear();

            var count = reader.ReadUInt16();

            for (ushort i = 0; i < count; i++)
            {
                var item = reader.ReadString();
                _items.Add(item);

                if (item == null)
                    _freeIds.Push(i);
            }
        }

        public void CopyFrom(ClientIdCollection tempNewClientIdCollection)
        {
            Clear();

            for (var i = 0; i < tempNewClientIdCollection._items.Count; i++)
            {
                var item = tempNewClientIdCollection._items[i];

                _items.Add(item);

                if (item == null)
                    _freeIds.Push((ushort)i);
            }
        }
    }

    internal interface IReadonlyClientIdCollection
    {
        ushort? GetId(string player);

        [CanBeNull] string GetName(ushort id);
    }
}
