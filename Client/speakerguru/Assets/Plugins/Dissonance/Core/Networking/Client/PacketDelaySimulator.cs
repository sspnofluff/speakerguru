﻿using System;
using System.Collections.Generic;
using System.Threading;
using Dissonance.Config;

namespace Dissonance.Networking.Client
{
    internal class PacketDelaySimulator
    {
        #region helper types
        private struct DelayedPacket
        {
            public readonly ArraySegment<byte> Data;
            public readonly DateTime SimulatedReceiptTime;

            public DelayedPacket(ArraySegment<byte> data, DateTime simReceiptTime)
            {
                Data = data;
                SimulatedReceiptTime = simReceiptTime;
            }
        }
        #endregion

        #region fields and properties
        private readonly Random _rnd = new Random();
        private int _delayedCount;
        private readonly List<DelayedPacket> _delayedUnorderedPackets = new List<DelayedPacket>();
        private readonly Queue<DelayedPacket> _delayedOrderedPackets = new Queue<DelayedPacket>();

        private readonly List<DelayedPacket> _processingPackets = new List<DelayedPacket>();
        private readonly List<ArraySegment<byte>> _tmpResult = new List<ArraySegment<byte>>();
        #endregion

        public bool EnqueuePacket(ArraySegment<byte> data, DateTime? now = null)
        {
            if (data.Array == null)
                throw new ArgumentNullException("data");

            if (!DebugSettings.Instance.EnableNetworkSimulation)
                return false;

            var reader = new PacketReader(data);
            reader.ReadUInt16(); // skip magic number (we don't want to error check it here, we'll let the error get caught in the normal way)
            var header = (MessageTypes)reader.ReadByte();

            //Check if we should discard this packet to fake packet loss
            if (!IsReliable(header))
            {
                var lossRoll = _rnd.NextDouble();
                if (lossRoll < DebugSettings.Instance.PacketLoss)
                    return true;
            }

            var delay = _rnd.Next(DebugSettings.Instance.MinimumLatency, DebugSettings.Instance.MaximumLatency);
            var simulatedArrivalTime = (now ?? DateTime.Now) + TimeSpan.FromMilliseconds(delay);

            var dataCopy = new byte[data.Count];
            Array.Copy(data.Array, data.Offset, dataCopy, 0, data.Count);

            var packet = new DelayedPacket(
                new ArraySegment<byte>(dataCopy),
                simulatedArrivalTime
            );

            Interlocked.Increment(ref _delayedCount);
            if (IsOrdered(header))
            {
                lock (_delayedOrderedPackets)
                    _delayedOrderedPackets.Enqueue(packet);
            }
            else
            {
                lock (_delayedUnorderedPackets)
                    _delayedUnorderedPackets.Add(packet);
            }

            return true;
        }

        private static bool IsOrdered(MessageTypes header)
        {
            return header != MessageTypes.VoiceData;
        }

        private static bool IsReliable(MessageTypes header)
        {
            return header != MessageTypes.VoiceData;
        }

        public List<ArraySegment<byte>> GetDelayedPackets(DateTime? now = null)
        {
            _processingPackets.Clear();
            _tmpResult.Clear();

            //Early exit to ensure we do no work at all when delayed packet processing has not got anything in its's buffers
            //This means in the common case where delayed packet processing is simply not active we only do this single comparison
            if (_delayedCount == 0)
                return _tmpResult;

            var t = now ?? DateTime.Now;

            //Pull out all of the unordered packets which have been sufficiently delayed
            lock (_delayedUnorderedPackets)
            {
                for (var i = _delayedUnorderedPackets.Count - 1; i >= 0; i--)
                {
                    var p = _delayedUnorderedPackets[i];
                    if (p.SimulatedReceiptTime <= t)
                    {
                        _processingPackets.Add(p);
                        _delayedUnorderedPackets.RemoveAt(i);
                    }
                }
            }

            //Sort the unordered packets by their receipt time
            _processingPackets.Sort((a, b) => a.SimulatedReceiptTime.CompareTo(b.SimulatedReceiptTime));

            //Keep dequeuing ordered packets which they have been sufficiently delayed (ensuring that we preserve order)
            lock (_delayedOrderedPackets)
            {
                while (_delayedOrderedPackets.Count > 0 && _delayedOrderedPackets.Peek().SimulatedReceiptTime <= t)
                    _processingPackets.Add(_delayedOrderedPackets.Dequeue());
            }

            //Ensure the counter is kept up to date
            Interlocked.Add(ref _delayedCount, -_processingPackets.Count);

            //Move over to result list
            for (var i = 0; i < _processingPackets.Count; i++)
                _tmpResult.Add(_processingPackets[i].Data);

            return _tmpResult;
        }
    }
}
