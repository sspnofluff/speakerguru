﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UInterface
{
	public Vector3 globalPos;
	public Vector3 globalRot;

}

public class UIManager : MonoBehaviour 
{
	public UIInterfaceOne source;
	public UInterface[] listenerInterface;
}
