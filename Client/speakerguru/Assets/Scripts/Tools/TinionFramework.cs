﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct CounterTimerF
{
	public bool trigger;

	public float current;
	public float max;
	public float maxOffset;
	public bool enableOffset;

	public CounterTimerF(float _current, float _max)
	{
		enableOffset = false;
		current = _current;
		max = _max;
		maxOffset = 0;
		trigger = false;
	}

	public CounterTimerF(float _max, float _maxOffset, bool _enableOffset)
	{
		enableOffset = _enableOffset;
		current = 0;
		max = _max;
		maxOffset = _maxOffset;
		trigger = false;
	}

	public bool isEnd(float _step)
	{
		current -= _step;
		if (current < 0)
		{
			Restore();
			return true;
		}
		return false;
	}

	public void Restore()
	{
		trigger = false;
		current = max;
		if (enableOffset)
			current = Random.Range(max - maxOffset, max + maxOffset);
	}

	public string GetToolTipInfo(string _two, string _one)
	{
		string result = "";
		if (enableOffset)
		{
			result += (max - maxOffset).ToString(_two);
			result += " - ";
			result += (max + maxOffset).ToString(_two);
		}
		else
			result += max.ToString(_one);

		return result;
	}
}

[System.Serializable]
public class Math3d
{
	public static Vector3 CubicBezierLerp(Vector3 start, Vector3 p1, Vector3 p2, Vector3 end, float t)
	{
		float u = 1.0f - t;
		float tt = t * t;
		float uu = u * u;
		float uuu = uu * u;
		float ttt = tt * t;

		Vector3 p = start * uuu;   
		p += (p1 * (3 * uu * t));    
		p += (p2 * (3 * u * tt));    
		p += (end * ttt);          

		return p;
	}
}

public class Draw
{
	public static void DrawCircle(Vector3 pos, float r, Color color, float duration)
	{
		for (int i = 0; i <= 20; i++)
		{
			float theta = 2.0f * 3.1415925f * (float)i / 20.0f;
			float theta2 = 2.0f * 3.1415925f * (float)(i + 1) / 20.0f;

			float x = r * Mathf.Cos(theta);
			float y = r * Mathf.Sin(theta);
			float x2 = r * Mathf.Cos(theta2);
			float y2 = r * Mathf.Sin(theta2);

			Debug.DrawLine(new Vector3(x + pos.x, pos.y, y + pos.z), new Vector3(x2 + pos.x, pos.y, y2 + pos.z), color, duration);//output vertex
		}

	}

	public static void DrawCircle(Vector3 pos, float r, Color color)
	{
		for (int i = 0; i <= 20; i++)
		{
			float theta = 2.0f * 3.1415925f * (float)i / 20.0f;
			float theta2 = 2.0f * 3.1415925f * (float)(i + 1) / 20.0f;

			float x = r * Mathf.Cos(theta);
			float y = r * Mathf.Sin(theta);
			float x2 = r * Mathf.Cos(theta2);
			float y2 = r * Mathf.Sin(theta2);

			Debug.DrawLine(new Vector3(x + pos.x, pos.y, y + pos.z), new Vector3(x2 + pos.x, pos.y, y2 + pos.z), color);//output vertex
		}

	}
}

