﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Size
{
	size66,
	size71,
}

[System.Serializable]
public class Emotion
{
	public UIButtonType type;
	public string[] text;
	public Size size;
	public Sprite icon;
}

[CreateAssetMenu(fileName="EmotionData", menuName="Resources/List", order = 1)]
public class EmotionalData : ScriptableObject
{
	

	public Emotion[] emotion;
}
