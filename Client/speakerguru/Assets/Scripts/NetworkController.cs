﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class NetworkController : NetworkBehaviour 
{

	[SyncVar(hook = "OnChangeRand")]
	public int randPos;

	void OnChangeRand (int randPos)
	{
		Debug.Log (randPos + " Rand");
	}

	public override void OnStartServer ()
	{
		if (isServer)
		randPos = -2;
	}

	//public override void OnStartClient ()
	//{
	//	randPos++;
	//}
	public void OnConnectionListener(int _randPos)
	{
		if (isServer) 
		{
			randPos = _randPos;
			Debug.Log ("ListenerConnect");
		}
	}
}
