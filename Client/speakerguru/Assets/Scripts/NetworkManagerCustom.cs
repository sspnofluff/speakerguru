﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;


public class NetworkManagerCustom : NetworkManager {

	public static NetworkManagerCustom instance;
	//public NetworkController controller;
	public int currentID = 0;

	void Awake()
	{
		instance = this;
	}

	public void Start()
	{
		//UnityEngine.VR.InputTracking.GetLocalRotation(
		//MenuManager.instance.SpeakerClick ();
		//UnityEngine.VR.VRSettings.enabled = true;
		//UnityEngine.VR.VRSettings.LoadDeviceByName ("Cardboard");
	}

	public override void OnServerAddPlayer (NetworkConnection conn, short playerControllerId)
	{
		base.OnServerAddPlayer (conn, playerControllerId);
		//Debug.Log ("Add Listener " + playerControllerId);
		//controller.randPos++;
		//controller.OnConnectionListener (controller.randPos);
	}

		
	public override void OnServerDisconnect (NetworkConnection conn)
	{
		base.OnServerDisconnect (conn);
		Debug.Log ("OnServerDisconnect");
		currentID--;
	}


}
