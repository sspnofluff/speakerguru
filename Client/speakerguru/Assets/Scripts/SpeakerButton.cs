﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Barebones.MasterServer;

public class SpeakerButton : DirectButton {

	bool isClicked = false;
	public CreateGameUi createGameUI;

	public override void OnClickTime()
	{
		if (isClicked == false) 
		{
			createGameUI.OnCreateClick ();
			isClicked = true;
			VRStartUI._instance.CanvasVR.gameObject.SetActive (false);
		}
		//MenuManager.instance.SpeakerClick ();
	}
				
}
