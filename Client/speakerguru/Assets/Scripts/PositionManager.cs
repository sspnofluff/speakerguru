﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionManager : MonoBehaviour 
{
	public static PositionManager instantiate;
	public Transform speakerPosition;
	public Transform[] listenerPositions;

	void Awake()
	{
		instantiate = this;
	}
}
