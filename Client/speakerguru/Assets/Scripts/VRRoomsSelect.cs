﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Barebones.MasterServer;

public class VRRoomsSelect : MonoBehaviour 
{
	public static VRRoomsSelect _instance;

	public int select;

	public List<CreateGameUi.MapSelection> Maps;
	public RoomButton[] buttons;

	void Awake()
	{
		_instance = this;

		for (int i = 0; i < buttons.Length; i++) 
		{
			if (buttons [i].orderNum == select) 
			{
				MenuManager.current = buttons [i];
			}
		}
	}

	public void SelectRoom(int _select)
	{
		select = _select;

		for (int i = 0; i < buttons.Length; i++) 
		{
			if (buttons [i].orderNum == _select) 
			{
				buttons [i]._selection.enabled = true;

			}
				else 
			{
				buttons [i]._selection.enabled = false;
			}
		}
	}
	

}
