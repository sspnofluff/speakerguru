﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HandTimerState
{
	Free,
	Grab,
	Motion,
}

[System.Serializable]
public class HandTimer
{
	public HandTimerState handTimerState;
	public float timerCurrent;
	public float timerMax;	
	public float timerGrabCurrent;
	public float timerGrabMax;
	public float weight;

	private bool isToFree;

	public void Change(AnimationCurve _ac)
	{
		switch (handTimerState) 
		{
		case HandTimerState.Free:
			weight = 0;
			break;
		case HandTimerState.Grab:
			weight = 1;
			break;
		case HandTimerState.Motion:
			timerCurrent -= Time.deltaTime * 2;

			if (isToFree)
				weight = _ac.Evaluate(timerCurrent / timerMax);
			else
				weight = _ac.Evaluate(1 - timerCurrent / timerMax);

			if (timerCurrent <= 0) 
			{
				timerCurrent = timerMax;
				if (isToFree)
					handTimerState = HandTimerState.Free;
				else
					handTimerState = HandTimerState.Grab;
			}
			break;
		}
	}

	public void ToFree()
	{
		handTimerState = HandTimerState.Motion;
		timerCurrent = timerMax;
		isToFree = true;
	}

	public void ToGrab()
	{
		handTimerState = HandTimerState.Motion;
		timerCurrent = timerMax;
		isToFree = false;
	}

	public void WaitGrab()
	{
		timerGrabCurrent -= Time.deltaTime;
		if (timerGrabCurrent <= 0) 
		{
			timerGrabCurrent = timerGrabMax;
			ToGrab();
		}
	}
}

public class IKController : MonoBehaviour {

	public bool isIkLeft;
	public bool isIkRight;
	public Human human;
	public AnimationCurve ac;

	public Animator animator;

	public HandTimer left;
	public HandTimer right;

	public AudioSource applause;

	void Start()
	{
		animator = GetComponent<Animator> ();
	}

	public void ApplauseSound()
	{
		applause.Play ();
	}

	public void OffAnimation()
	{
		animator.SetBool("handUp", false);
		animator.SetBool("thumbUp", false);
		animator.SetBool("thumbDown", false);
		animator.SetBool("egg", false);
	}

	void Update()
	{
		if (human.rotateHead3 < 45 && human.rotateHead3 > -12 && left.handTimerState == HandTimerState.Free)
			left.WaitGrab ();

		if ((human.rotateHead3 >= 45 || human.rotateHead3 <= -12) && left.handTimerState == HandTimerState.Grab)
			left.ToFree ();

		if (human.rotateHead3 > -45 && human.rotateHead3 < 12 && right.handTimerState == HandTimerState.Free)
			right.WaitGrab ();

		if ((human.rotateHead3 <= -45 || human.rotateHead3 >= 12) && right.handTimerState == HandTimerState.Grab)
			right.ToFree ();

		left.Change (ac);
		right.Change (ac);
	}

	void OnAnimatorIK(int layerIndex)
	{
		
		if (isIkLeft) 
		{
			animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, left.weight);
			animator.SetIKPosition(AvatarIKGoal.LeftHand, MenuManager.instance.leftHandPodium.position);
			animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, left.weight);
			animator.SetIKRotation(AvatarIKGoal.LeftHand, MenuManager.instance.leftHandPodium.rotation);
		}

		if (isIkRight) 
		{
			animator.SetIKPositionWeight(AvatarIKGoal.RightHand, right.weight);  
			animator.SetIKPosition(AvatarIKGoal.RightHand, MenuManager.instance.rightHandPodium.position);
			animator.SetIKRotationWeight(AvatarIKGoal.RightHand, right.weight);  
			animator.SetIKRotation(AvatarIKGoal.RightHand, MenuManager.instance.rightHandPodium.rotation);
		}
	}



}
