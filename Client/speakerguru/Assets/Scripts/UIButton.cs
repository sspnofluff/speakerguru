﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UIButtonState
{
	Default,
	OnHidden,
	OnAppear,
	OnWait,
}

public enum UIButtonType
{
	NotUnderstand,
	BadJoke,
	Boooring,
	Funny,
	Interesting,
	Welldone,
	Wow,
	Wtf,
	ThumbUp,
	Applause,
	Hand,
	Egg,
	ThumbDown
}

public class UIButton : MonoBehaviour {

	private bool isEntered = false;
	private float timerCurrent;
	private RectTransform rTransform;

	public bool buttonTypeHorizontal = true;
	public UIInterfaceOne uiinterfaceone;

	public float timerMax = 2.0f;
	public float speed = 1.0f;
	public Vector2 animatePositions;

	public AnimationCurve animCurve;
	public UIButtonState state = UIButtonState.Default;
	public UIButtonType type;




	public void Start()
	{
		rTransform = GetComponent<RectTransform> ();
	}

	public void OnClick()
	{
		if (state == UIButtonState.Default) 
		{
			timerCurrent = timerMax;
			state = UIButtonState.OnHidden;
			uiinterfaceone.human.CmdSendEmotion (type);
		}
	}

	public void LateUpdate()
	{
		switch (state)
		{
		case UIButtonState.OnHidden:
			{
				timerCurrent -= Time.deltaTime * speed;
				Vector2 ancPos = rTransform.anchoredPosition;
				float currentPos = Mathf.Lerp (animatePositions.x, animatePositions.y, animCurve.Evaluate(timerCurrent / timerMax));
				if (buttonTypeHorizontal)
					ancPos.x = currentPos;
				else
					ancPos.y = currentPos;
					
			rTransform.anchoredPosition = ancPos;
			if (timerCurrent <= 0) 
			{
				state = UIButtonState.OnAppear;
				timerCurrent = timerMax;
			}
			} 
			break;
		case UIButtonState.OnAppear:
			{
				timerCurrent -= Time.deltaTime * speed;
				Vector2 ancPos = rTransform.anchoredPosition;
				float currentPos = Mathf.Lerp (animatePositions.y, animatePositions.x, animCurve.Evaluate(timerCurrent / timerMax));
				if (buttonTypeHorizontal)
					ancPos.x = currentPos;
				else
					ancPos.y = currentPos;
				
				rTransform.anchoredPosition = ancPos;
				if (timerCurrent <= 0) 
				{
					state = UIButtonState.Default;
					timerCurrent = timerMax;
				}
			}
			break;
		}
	}
}
