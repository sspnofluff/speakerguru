﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectButton : MonoBehaviour {

	public float timerMax = 2.0f;
	public float timerCurrent;
	protected bool isEntered = false;
	public bool isDisabled = false;

	public void Awake()
	{
		timerCurrent = timerMax;
	}

	public void OnEnterPointer()
	{
		if (isDisabled)
			return;
		
		MenuManager.current = this;
		isEntered = true;
	}

	public void OnExitPointer()
	{
		if (isDisabled)
			return;

		isEntered = false;
		timerCurrent = timerMax;
	}

	public virtual void OnClickTime()
	{
		MenuManager.current = null;
	}

	public virtual void Update()
	{
		if (isDisabled)
			return;
		
		if (isEntered) 
		{
			timerCurrent -= Time.deltaTime;
			if (timerCurrent <= 0) 
			{
				OnClickTime ();
			}
		}
	}
}
