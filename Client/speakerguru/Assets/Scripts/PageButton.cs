﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Barebones.MasterServer;

public class PageButton : DirectButton {

	public bool isLeft;



	public override void OnClickTime()
	{
		base.OnClickTime ();
		Paroxe.PdfRenderer.Examples.PDFDocumentRenderToTextureExample._instance.PageClick (isLeft);
		isEntered = false;
		timerCurrent = timerMax;
		MenuManager.current = this;
		isEntered = true;
	}

	/*
	public override void Update()
	{
		base.Update ();
		if (isLeft) {
			if (Paroxe.PdfRenderer.Examples.PDFDocumentRenderToTextureExample._instance.m_Page == 0)
				isDisabled = true;
			else
				isDisabled = false;
		} else 
		{
			if (Paroxe.PdfRenderer.Examples.PDFDocumentRenderToTextureExample._instance.m_Page == Paroxe.PdfRenderer.Examples.PDFDocumentRenderToTextureExample._instance.pageCount - 1)
				isDisabled = true;
			else
				isDisabled = false;
		}
	}
	*/			
}
