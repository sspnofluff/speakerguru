﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingImage : MonoBehaviour {

	public UnityEngine.UI.Image Image01;
	public UnityEngine.UI.Image Image02;

	// Update is called once per frame
	private void Update()
	{
		Image01.transform.Rotate(Vector3.forward, Time.deltaTime*360*2);
		Image02.transform.Rotate(Vector3.back, Time.deltaTime*360*3);
	}
}
