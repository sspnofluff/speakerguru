﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Barebones.MasterServer;
using Barebones.Networking;

public class VRStartUI : MonoBehaviour {

	public static VRStartUI _instance;
	protected static ConnectionStatus LastStatus;
	public GamesListUi gameListUI;
	private IClientSocket _connection;
	public Transform CanvasVR;

	public GameObject LoadingVR;
	public GameObject ConnectionErrorVR;

	void Awake()
	{
		_instance = this;
	}

	protected virtual void Start()
	{
		
		_connection = GetConnection();
		_connection.StatusChanged += UpdateStatusView;

		UpdateStatusView(_connection.Status);
	}

	protected virtual void UpdateStatusView(ConnectionStatus status)
	{
		LastStatus = status;

		switch (status)
		{
		case ConnectionStatus.Connected:

			LoadingVR.SetActive (false);
			ConnectionErrorVR.SetActive (false);

			var promise = Msf.Events.FireWithPromise (Msf.EventNames.ShowLoading, "Logging in");
			Msf.Client.Auth.LogInAsGuest ((accInfo, error) => {
				promise.Finish ();

				if (accInfo == null) {
					Msf.Events.Fire (Msf.EventNames.ShowDialogBox, DialogBoxData.CreateError (error));
					Logs.Error (error);
				}
			});

			gameListUI.OnCreateGameClick ();

			CanvasVR.gameObject.SetActive (true);
			break;
		case ConnectionStatus.Disconnected:

			ConnectionErrorVR.SetActive (true);
			LoadingVR.SetActive (false);
			CanvasVR.gameObject.SetActive (false);

			break;
		case ConnectionStatus.Connecting:

			ConnectionErrorVR.SetActive (false);
			LoadingVR.SetActive (true);
			CanvasVR.gameObject.SetActive (false);

			break;
		default:
			
			break;
		}
	}

	protected virtual IClientSocket GetConnection()
	{
		return Msf.Connection;
	}

	protected virtual void OnDestroy()
	{
		_connection.StatusChanged -= UpdateStatusView;
	}
}
