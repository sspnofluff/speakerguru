﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomButton : DirectButton {

	public int orderNum;
	public UnityEngine.UI.Image _selection;
	public UnityEngine.UI.Image current;
	public bool isCurrentAtStart;

	void Start()
	{
		if (isCurrentAtStart) 
		{
			MenuManager.current = this;
			isEntered = true;
		}
		
		if (isDisabled)
			current.color = new Color (0.3f, 0.3f, 0.3f, 1.0f);
		
		if (VRRoomsSelect._instance.select == orderNum) 
		{
			isEntered = true;
			MenuManager.current = this;
			OnClickTime ();
			timerCurrent = 0;
		}
	}

	public override void OnClickTime()
	{
		VRRoomsSelect._instance.SelectRoom (orderNum);
		base.OnClickTime ();
	}

//	public override void OnEnterPointer()
//	{
//		if (VRRoomsSelect._instance.select!=orderNum)
//			isEntered = true;
//	}

}
