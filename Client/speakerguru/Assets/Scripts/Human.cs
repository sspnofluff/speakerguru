﻿using UnityEngine;
using UnityEngine.Networking;

public enum HumanType
{
	Undefined,
	Listener,
	Speaker,
}

public class Human : NetworkBehaviour
{
	public Transform mainCamera;
	public Transform head;
	public Transform rightEye;
	public Transform leftEye;
	public Transform localTransformCamera;
	public Transform localTransform;
	public Transform root;
	public bool isInitialize = false;
	public bool isCamActivate = false;
	public CounterTimerF applauseTimer;

	[SyncVar]
	public Quaternion CamRotate;
	[SyncVar]
	public Vector3 CamForward;

	public RuntimeAnimatorController listener;
	public RuntimeAnimatorController speaker;

	public IKController ikController;

	public Vector3 headRotationDamp = Vector3.zero;
	Vector3 oldRotate = Vector3.zero;

	[SyncVar]
	public int randPos = -1;

	public HumanType humanType = HumanType.Undefined;

	public Animator animator;
	public BillboardBubble billboardBubble;

	public float rotateHead;
	public float rotateHead3;

	public Quaternion rotateEmoteStart;
	public Quaternion rotateEmoteEnd;

    void Update()
    {
		if (!isInitialize)
			Initialize ();

		//пока активно - играет анимация аплодисментов
		if (applauseTimer.trigger)
			if (applauseTimer.isEnd(Time.deltaTime))
				{
					animator.SetBool("applause", false);
				}

		//захват VR-камеры
		//#if UNITY_ANDROID
		//if (humanType == HumanType.Speaker)
		//{
	//		mainCamera.transform.rotation = UnityEngine.VR.InputTracking.GetLocalRotation(UnityEngine.VR.VRNode.Head);
		//	mainCamera.transform.Rotate(Vector3.up, 180f);
		//}
		//#endif

		//Обнуление перемещений, связанных с Blend-tree
		animator.transform.localPosition = Vector3.zero;

		if (isLocalPlayer) 
		{
			//коррекция положения камеры головы для спикера (он сидит)
			if (humanType == HumanType.Listener) 
			{
					Vector3 localRot = mainCamera.transform.localEulerAngles;
					localRot.y = CorrectAngle (localRot.y);
					localRot = new Vector3 (localRot.x, Mathf.Clamp (localRot.y, -110f, 110f), localRot.z);
					mainCamera.transform.localEulerAngles = localRot;
			}

			CamForward = mainCamera.transform.forward;
			CamRotate = mainCamera.transform.rotation;
			CmdSendInputData (mainCamera.transform.forward, mainCamera.transform.rotation);

			//скрывание головы для протагониста (чтобы не было видно задника губ, носа из камеры данного аватара) 
			head.transform.localScale = Vector3.zero;
		}

		rotateHead = GetAngle (root.forward.z, root.forward.x, CamForward.z, CamForward.x);
		rotateHead3 = GetAngle (transform.forward.z, transform.forward.x, root.transform.forward.z, root.forward.x);

		if (Mathf.Abs (rotateHead) > 35.0f)
			animator.SetFloat ("Turn", rotateHead / 90.0f, 0.1f, Time.deltaTime);
		else
			animator.SetFloat ("Turn", 0, 0.1f, Time.deltaTime);

		if (!isCamActivate) 
		{
			isCamActivate = true;
			MenuManager.instance.backGround.gameObject.SetActive (false);
		}
	}

	[Command]
	public void CmdSendInputData(Vector3 _forward, Quaternion _rotation)
	{
		if (!isServer)
			return;
		
		CamForward = _forward;
		CamRotate = _rotation;
		RpcSendRotateData (_forward, _rotation);
	}

	[ClientRpc]
	public void RpcSendRotateData(Vector3 _forward, Quaternion _rotation)
	{
		if (isServer)
			return;
		
		if (!isLocalPlayer) 
		{
			CamForward = _forward;
			CamRotate = _rotation;
		}
	}

	public static float CorrectAngle(float angle)
	{
		float result = angle;
		if (result > 180)
			result -= 360.0f;
		else if (result < -180)
			result += 360.0f;
		return result;
	}

	void LateUpdate()
	{
		rightEye.rotation = CamRotate;
		leftEye.rotation = CamRotate;
		Vector3 rightEyeLocalRotation = rightEye.localEulerAngles;
		Vector3 leftEyeLocalRotation = leftEye.localEulerAngles;
		rightEye.localRotation = Quaternion.Euler (Mathf.Clamp (CorrectAngle (rightEyeLocalRotation.x), -14f, 14f), Mathf.Clamp (CorrectAngle (rightEyeLocalRotation.y), -34f, 34f), 0);
		leftEye.localRotation = Quaternion.Euler (Mathf.Clamp (CorrectAngle (leftEyeLocalRotation.x), -14f, 14f), Mathf.Clamp (CorrectAngle (leftEyeLocalRotation.y), -34f, 34f), 0); 

		Vector3 rotationLocal;
		Vector3 old = CamRotate.eulerAngles;

		//поправка для захвата VR камеры
		//#if UNITY_ANDROID
		//if (humanType == HumanType.Speaker)
		//	old.x *= -1;
		//#endif

		//замедление поворота головы
		rotationLocal.x = Mathf.SmoothDampAngle (oldRotate.x, old.x, ref headRotationDamp.x, 0.25f);
		rotationLocal.y = Mathf.SmoothDampAngle (oldRotate.y, old.y, ref headRotationDamp.y, 0.25f);
		rotationLocal.z = Mathf.SmoothDampAngle (oldRotate.z, old.z, ref headRotationDamp.z, 0.25f);
		head.rotation = Quaternion.Euler (rotationLocal);
		oldRotate = head.rotation.eulerAngles;

		Vector3 newHeadPosition = root.transform.position - head.transform.position;
		newHeadPosition.y = 0;
		localTransform.localPosition = newHeadPosition;

	}

			
	public static float Angle(Vector2 from, Vector2 to)
	{
		float angle = Mathf.Atan2 (from.y - to.y, from.x - to.x); // в радианах
		return angle * (180/Mathf.PI); 
	}

	public static float GetAngle(float Ax, float Ay, float Bx, float By)
	{
		float result;
		result = Mathf.Atan2(Ax * By - Bx * Ay, Ax * Bx + Ay * By);
		result *= Mathf.Rad2Deg;
		return result;
	}

	public static float GetAngle(float Ax, float Ay)
	{
		float result;
		result = Mathf.Atan2(Ax * Vector3.forward.x - Vector3.forward.z * Ay, Ax * Vector3.forward.z + Ay * Vector3.forward.x);
		result *= Mathf.Rad2Deg;
		return result;
	}

	public void Initialize()
	{
		if (humanType == HumanType.Speaker) 
		{
			if (isServer)
			{
				NetworkManagerSample.instance.clientManager = new ClientManager (4);
			}
			isInitialize = true;
			Debug.Log ("Speaker to Pos");
			transform.position = PositionManager.instantiate.speakerPosition.position;
			transform.rotation = PositionManager.instantiate.speakerPosition.rotation; 
			animator.runtimeAnimatorController = speaker;
			ikController.isIkLeft = true;
			ikController.isIkRight = true;

		}
			else
		if (humanType == HumanType.Listener) 
		{
			isInitialize = true;
			
			applauseTimer = new CounterTimerF (2.5f, 2.5f);
			
			//отключение реалтаймовой инверсной кинематики у слушателя
			ikController.isIkLeft = false;
			ikController.isIkRight = false;
			
			//корректировка положения камеры у слушателя
			Vector3 localPos = localTransformCamera.localPosition;
			localPos.y -= 0.31f;
			localPos.z -= 0.28f;
			localTransformCamera.localPosition = localPos;
			
			//запрет поворота внутренного рута (костыль, он почему-то при инстансе иногда поворачивается, не было времени разбираться почему)
			root.transform.localRotation = Quaternion.identity;
			
			//Инстансирование интерфейса слушателя
			if (isLocalPlayer) 
			{
				CmdListenerLocation();
 
				//Инстансирование интерфейса слушателя
				if (BuildType.clientType == ClientType.Default) 
				{
					UIInterfaceOne listenerUI = GameObject.Instantiate (MenuManager.instance.defaultClientSource) as UIInterfaceOne;
					listenerUI.human = this;
				}
			}
					
			animator.runtimeAnimatorController = listener;
		}
	}

	public void Start()
	{
		//mainCamera.gameObject.SetActive (isCamActivate);
		//Обновление всех хуманов, в том числе закинутых сервером до этого момента
		Human[] humans = FindObjectsOfType<Human> ();
		for (int i=0; i<humans.Length; i++)
			humans[i].HumanStart ();

		Paroxe.PdfRenderer.Examples.PDFDocumentRenderToTextureExample._instance.StartThis ();
	}

	public static void StartPDF()
	{
		Human[] humans = FindObjectsOfType<Human> ();
		for (int i=0; i<humans.Length; i++)
			if (humans[i].humanType == HumanType.Speaker)
				humans[i].CmdStartPDFLocal (Paroxe.PdfRenderer.Examples.PDFDocumentRenderToTextureExample._instance.pdfDocument.DocumentBuffer);
	}

	[Command]
	public void CmdStartPDFLocal(byte[] bytes)
	{
		if (!isServer)
			return;
		
		Debug.Log ("CMD Local");
		Paroxe.PdfRenderer.Examples.PDFDocumentRenderToTextureExample._instance.pdfDocument = new Paroxe.PdfRenderer.PDFDocument (bytes);
		Paroxe.PdfRenderer.Examples.PDFDocumentRenderToTextureExample._instance.UpdatePDFImage ();
		RpcStartPDFLocal (bytes);
	}

	[ClientRpc]
	public void RpcStartPDFLocal(byte[] bytes)
	{
		if (isServer)
			return;

		Paroxe.PdfRenderer.Examples.PDFDocumentRenderToTextureExample._instance.pdfDocument = new Paroxe.PdfRenderer.PDFDocument (bytes);
		Paroxe.PdfRenderer.Examples.PDFDocumentRenderToTextureExample._instance.UpdatePDFImage ();
		Debug.Log ("RPC Local");
	}

	public void HumanStart()
	{
		if (isLocalPlayer) 
		{
			if (MenuManager.instance.humanType == HumanType.Speaker) 
			{
				humanType = HumanType.Speaker;
				CmdStartSpeaker (humanType);
			} 
			else
				if (MenuManager.instance.humanType == HumanType.Listener) 
				{
					humanType = HumanType.Listener;
					CmdStartSpeaker (humanType);
				}
		}

		if (!isInitialize)
		Initialize ();
	}

	public override void OnStartLocalPlayer()
	{
		//перемещение камеры на место головы аватара
		mainCamera = Camera.main.transform;
		mainCamera.parent = localTransform;
		mainCamera.localPosition = Vector3.zero;
		mainCamera.localRotation = Quaternion.identity;
	}

	[Command]
	public void CmdStartSpeaker(HumanType _type)
	{
		if (!isServer)
			return;
		
		Debug.Log ("Server " + humanType.ToString ());
		humanType = _type;
		RpcStartSpeaker (_type);
	}

	[ClientRpc]
	public void RpcStartSpeaker(HumanType _type)
	{
		if (isServer)
			return;

		Debug.Log ("Client " + humanType.ToString ());
		if (!isLocalPlayer)
			humanType = _type;
	}

	[Command]
	public void CmdListenerLocation()
	{
		if (!isServer)
			return;

		//распределение Слушателя на его место
		randPos = NetworkManagerSample.instance.clientManager.GetClientNumber ();
		transform.position = PositionManager.instantiate.listenerPositions[randPos].position;
		transform.rotation = PositionManager.instantiate.listenerPositions[randPos].rotation;
		RpcListenerLocation (randPos);
	}

	[ClientRpc]
	public void RpcListenerLocation(int _currentID)
	{
		if (isServer)
			return;

		//распределение Слушателя на его место
		randPos = _currentID;
		if (isLocalPlayer) 
		{
			transform.position = PositionManager.instantiate.listenerPositions[randPos].position;
			transform.rotation = PositionManager.instantiate.listenerPositions[randPos].rotation;

		}
	}

	[Command]
	public void CmdSendEmotion(UIButtonType src)
	{
		if (!isServer)
			return;
		
		RpcSendEmotion (src);
	}

	[ClientRpc]
	public void RpcSendEmotion(UIButtonType src)
	{
		if (isServer)
			return;

		//Включение пузыря с текстом у спикера
		if (MenuManager.instance.humanType == HumanType.Speaker)
		{
			if (src != UIButtonType.Applause && src != UIButtonType.Egg && src != UIButtonType.Hand && src != UIButtonType.ThumbDown && src != UIButtonType.ThumbUp)
			billboardBubble.Activate (src);
		}

		//Включение анимаций-эмоций
		switch (src) 
		{
		case UIButtonType.Applause:
			animator.SetBool ("applause", true);
			applauseTimer.trigger = true;
			break;
		case UIButtonType.Egg:
			animator.SetBool ("egg", true);
			break;
		case UIButtonType.Hand:
			animator.SetBool ("handUp", true);
			break;
		case UIButtonType.ThumbUp:
			animator.SetBool ("thumbUp", true);
			break;
		case UIButtonType.ThumbDown:
			animator.SetBool ("thumbDown", true);
			break;
		default:
			break;
		
		}
	}

	//Освобождение места Слушателя на сервере
	void OnDestroy()
	{
		if (isServer) 
		{
			if (randPos>=0)
			NetworkManagerSample.instance.clientManager.FreeClientNumber (randPos);
		}
	}
		
}