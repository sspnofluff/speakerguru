﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
#if UNITY_EDITOR
using UnityEditor;
#endif

public enum ClientType
{
	VR,
	Default,
	Server,
}

public static class BuildType
{
	public static ClientType clientType = ClientType.Server;
	public static string masterServerIP = "127.0.0.1";
	public static int masterServerPort = 7777;
}

public class MenuManager : MonoBehaviour 
{
	public static MenuManager instance;
	public static DirectButton current;

	public GameObject VRCam;
	public GameObject DefaultCam;
	public GameObject pointer;
	public UIInterfaceOne defaultClientSource;

	public Transform leftHandPodium;
	public Transform rightHandPodium;
	public Transform backGround;

	public HumanType humanType;

	public void Awake()
	{
		instance = this;
	}

	void Start()
	{

		if (BuildType.clientType == ClientType.VR) 
		{
			humanType = HumanType.Speaker;
			VRCam.SetActive (true);
			//pointer.SetActive (false);
		} 
		else
			DefaultCam.SetActive (true);
		if (BuildType.clientType == ClientType.Default)
			humanType = HumanType.Listener;
		
		if (BuildType.clientType == ClientType.Server)
			humanType = HumanType.Undefined;
		
	}

	public void ListenerClick()
	{
		humanType = HumanType.Listener;
	}

	public void SpeakerClick()
	{
		humanType = HumanType.Speaker;
		//pointer.SetActive (false);
	}

	void Update()
	{
		if (Input.GetKeyUp  (KeyCode.O)) 
		{
			SpeakerClick ();
		}
		if (Input.GetKeyUp (KeyCode.P)) 
		{
			ListenerClick ();
		}
	}
}
