﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BillboardBubble : MonoBehaviour {

	public Image bubble;
	public Image smile;
	public UnityEngine.UI.Text text;
	public UIButtonState state;
	public float speed = 1.0f;

	public AnimationCurve animCurve;
	private bool isEntered = false;
	private float timerCurrent;
	public float timerMax = 2.0f;

	public EmotionalData emotionalData;

	void ChangeColor(float t)
	{
		Color color = new Color (1.0f, 1.0f, 1.0f, animCurve.Evaluate(t)); 
		bubble.color = color;
		smile.color = color;
		text.color = new Color (0.0f, 0.0f, 0.0f, animCurve.Evaluate(t));
	}

	public void Activate(UIButtonType type)
	{
		Emotion emotion = GetType (type);
		text.text = emotion.text [Random.Range (0, emotion.text.Length)];
		smile.sprite = emotion.icon;
		if (emotion.size == Size.size66)
			smile.rectTransform.sizeDelta = new Vector2 (66.0f, 66.0f);
		else
			smile.rectTransform.sizeDelta = new Vector2 (71.0f, 71.0f);
		state = UIButtonState.OnAppear;
		timerCurrent = timerMax;
	}

	public Emotion GetType(UIButtonType type)
	{
		for (int i = 0; i < emotionalData.emotion.Length; i++) 
		{
			if (type == emotionalData.emotion [i].type) 
			{
				return emotionalData.emotion [i];
			}
		}

		return null;
	}

	void Update () 
	{
		

		switch (state)
		{
		case UIButtonState.Default:
			{
				ChangeColor (0.0f);
				bubble.rectTransform.localScale = new Vector3 (-0.001f, 0.001f, 0.001f);
			}
			break;
		case UIButtonState.OnAppear:
			{
				ChangeColor (1.0f - timerCurrent / timerMax);
				float f = Mathf.Lerp (0.001f, 0.1f, 1.0f - timerCurrent / timerMax);
				bubble.rectTransform.localScale = new Vector3 (-f, f, f);
				timerCurrent -= Time.deltaTime * speed;
				if (timerCurrent <= 0) 
				{
					state = UIButtonState.OnWait;
					timerCurrent = timerMax * 2;
				}
			}
			break;
		case UIButtonState.OnWait:
			{
				ChangeColor (1.0f);
				bubble.rectTransform.localScale = new Vector3 (-0.1f, 0.1f, 0.1f);
				timerCurrent -= Time.deltaTime * speed;
				if (timerCurrent <= 0) 
				{
					state = UIButtonState.OnHidden;
					timerCurrent = timerMax;
				}
			} 
			break;
		case UIButtonState.OnHidden:
			{
				ChangeColor (timerCurrent / timerMax);
				float f = Mathf.Lerp (0.15f, 0.1f, timerCurrent / timerMax);
				bubble.rectTransform.localScale = new Vector3 (-f, f, f);
				timerCurrent -= Time.deltaTime * speed;
				if (timerCurrent <= 0) 
				{
					state = UIButtonState.Default;
					timerCurrent = timerMax;
				}
			}
			break;
		
		}

		transform.LookAt(Camera.main.transform);
	}
}
