﻿using UnityEngine;

namespace Paroxe.PdfRenderer.Examples
{
	public class PDFDocumentRenderToTextureExample : MonoBehaviour
    {
		public Texture2D windows;
		public Material currentMat;
        public int m_Page = 0;
		public int currentPage;
		public static PDFDocumentRenderToTextureExample _instance;

		public PDFDocument pdfDocument;
		public int pageCount;

#if !UNITY_WEBGL

		void Awake()
		{
			_instance = this;
		}

		public void StartThis()
		{

			if (BuildType.clientType == ClientType.VR) 		
			{
				pdfDocument = new PDFDocument (PDFBytesSupplierExample.PDFSampleByteArray, "");
				//pdfDocument = new PDFDocument("C:/Users/Develop/Desktop/Doc4.pdf");
			}

			UpdatePDFImage ();
			Human.StartPDF ();
		}
				
		public void UpdatePDFImage()
		{
			if (pdfDocument.IsValid) 
			{
				pageCount = pdfDocument.GetPageCount ();

				PDFRenderer renderer = new PDFRenderer ();
				Texture2D tex = renderer.RenderPageToTexture (pdfDocument.GetPage (m_Page % pageCount), 1024, 1024);

				tex.filterMode = FilterMode.Trilinear;
				tex.anisoLevel = 16;

				currentMat.mainTexture = tex;
				currentMat.SetTexture ("_EmissionMap", tex);
			} 
			else
			{
				currentMat.mainTexture = windows;
				currentMat.SetTexture ("_EmissionMap", windows);
			}
		}

		public void PageClick(bool isLeft)
		{
			if (isLeft) 
			{
				if (m_Page > 0)
					m_Page--;
			} 
			else 
				if (m_Page < pageCount - 1)
					m_Page++;

			
			UpdatePDFImage ();

		}
#endif
    }
}
